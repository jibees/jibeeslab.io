---
title: Expériences
description: 
images: ["/images/opengraph.jpg"]
---

Je fais ici la liste de mes expériences professionnelles salariées ou en freelance.

### OpenFoodNetwork
*à partir de janvier 2021* **Plateforme de vente pour les circuits courts alimentaires**

Développeur full stack Ruby on Rails/AngularJS.
Participation à la conception et au développement de la plateforme de vente en ligne pour les circuits courts alimentaires.

En savoir plus dans [mon portfolio]({{< ref "../portfolio/openfoodnetwork.org/index.md" >}} "mon portfolio").

```#github #angularjs #rubyonrails #postgresql #docker #ansible #githubactions```

### Linkfluence
*2018-2019* **Plateforme de recherche d’influenceurs**


Définir, architecturer et implémenter une Single Page Application
Tests unitaires, d'intégration et end-to-end

```#gitlab #es6 #typescript #react #redux #saga #jest #cypressio```


###  Hawkeye
*2017-2018* **Plateforme d’IA pour le content marketing**

##### Frontend
 - Définir, architecturer et implémenter une Single Page Application
 - Application type dashboard multi charts : améliorer constamment les performances (DOM, javascript, HTTP)
 - Tests unitaires, d'intégration et end-to-end

```#javascript #es6 #react #redux #saga #jest #webdriverio #jenkins #browserstack```

##### Backend
 - Définir, implémenter les APIs REST vers le frontend

```#java8 #spring #jpa #elasticsearch```


### Scoop.it
*2015-2017* **Plateforme d’outils de content marketing**
##### Frontend
 - Définir & réaliser les interfaces utilisateurs (création de newsletters, calendrier, planification de multi partages, …)

```#javascript #react```


###  Scoop.it
*2011-2015* **Application sociale de revue de web (curation)**
##### Backend
 - connecter les APIs tiers ; analytics ; systèmes d'événements

```#java #cassandra #hive```

##### Frontend
 - créer le site web ; implémenter de l’A/B testing

```#jQuery #ajax```

##### Mobile
 - réaliser l’application iOS / iPad

```#iOS #objectiveC```


### Goojet
*2008-2011* **web-mobile à forte dimension sociale**

##### Backend
 - définir les APIs avec le frontend

```#java #mysql #spring```

##### Frontend
 - définir, implémenter les applications sociales (partage de photos, rencontre, …)

```#jQuery #ajax```

### Goojet
*2007-2008* **solution de web-mobile**

##### Web
 - créer les widgets 

```#javascript #wicket```
##### Mobile
 - créer le prototype de l’application mobile ; définir le protocole et l’API

```#java #j2me```


###  Lucyde
*2006-2007* **intégrateur d’IPBX pour PME**
##### Ops
 - créer la distribution auto-installable  et préconfigurée basée sur ubuntu 

```#ubuntu #asterisk #voip ```
##### Web
 - créer l’interface de configuration du réseau VoIP

```#html #css #php```

###  INSA Toulouse
*2007*
 - Intervenant “Les nouvelles tendances du Web”
 - Cours, TD & TP aux étudiants de master sur la révolution du web 2.0 (social, travail collaboratif, personnalité numérique, …)

```#enseignement #pédagogie```
