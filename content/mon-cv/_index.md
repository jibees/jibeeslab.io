---
title: Mon CV
description: 
images: ["/images/cv.jpg"]
---

Vous trouverez ci-joint mon CV de développeur frontend expert react/redux. Celui-ci est très orienté technologies web, et ne prend pas en compte la diversité des compétences et des savoirs liés à ma [formation récente](<{{< relref "/formations#inp-toulouse" >}}>). Le site internet sur lequel vous êtes actuellement s'y prête [plus volontiers]({{< relref "/#compétences" >}}).

{{< figure src="/images/cv.jpg" link="/media/CV_Jean-Baptiste_Bellet_Developpeur_pour_les_transitions.pdf" >}}

