---
title: Formation
description: 
images: ["/images/opengraph.jpg"]
---

### INP Toulouse
*2019-2020* **Mastère spécialisé éco-ingénierie**

Le mastère a pour objectif de former des cadres capables d’appréhender les questions technologiques de manière systémique et approfondie, avec les outils de la modélisation et de l’évaluation. Prenant en compte les grands enjeux de notre temps (biodiversité, climat, énergie, ...), l'intelligence collective et le travail interdisciplinaire, ce mastère permet de former les ingénieurs aux transitions de demain.

Cette formation m'a permis d'acquérir une culture globale et systémique afin de : 
 - Répondre aux enjeux du développement *vraiment* durable
 - Inventer des solutions au service de la société et des environnements
 - Maîtriser des concepts, méthodes et outils systémiques
 - Porter des projets interdisciplinaires de développement durable
 - Concevoir et mettre en œuvre des projets moteurs des transitions vers de nouveaux modèles de développement.


### ESC Toulouse
*2006* **Master Management en Environnement HighTech**


### INSA Toulouse
*2003-2006* **Ingénieur département informatique, spécialité réseaux & télécoms**



###  IUT Blagnac
*2001-2003* **Génie des télécommunications et des réseaux**

