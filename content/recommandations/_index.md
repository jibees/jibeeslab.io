---
title: Mes recommandations
description: 
images: ["/images/opengraph.jpg"]
---
Durant mes 13 années d'expériences, j'ai eu l'occasion de rencontrer des personnes qui ont beaucoup compté pour moi, tant sur le plan professionnel que sur le plan personnel. J'ai ainsi demandé à trois d'entres elles, qui ont eu de l'influence sur ma carrière, si elles voulaient bien faire une recommandation concernant mon CV. Je reproduis ici leur recommandation, en contextualisant la rencontre.

### Vincent Demay
[{{% fontawesome linkedin %}}](https://www.linkedin.com/in/vdemay/)
**Lead software engineer** @ *[Linkfluence](https://www.linkfluence.com/)*

J'ai travaillé avec Vincent Demay pendant plus de 10 années ([Goojet]({{<relref "/expériences/#goojet" >}}), [scoop.it]({{<relref "/expériences/#scoopit" >}}), [Hawkeye]({{<relref "/expériences/#hawkeye" >}})) où il a été tour à tour un camarade d'école, mon mentor, mon manager et surtout un ami. 

> Jean-Baptiste est une personne très consciencieuse, en permanence à l'écoute de ses collègues, il sait appréhender et résoudre les problèmes aussi bien d'un point de vue technique qu'humain. Son ouverture d'esprit et son dynamisme ont participé activement à maintenir une très bonne ambiance au travail. Je recommande vivement Jean-Baptiste. 

### Marc Rougier 
[{{% fontawesome linkedin %}}](https://www.linkedin.com/in/marcrougier/)
**Entrepeneur, VC** @ *[Elaia Partners](https://www.elaia.com/marc-rougier/)*

Marc m'a embauché, sur recommandation, pour bâtir l'équipe initiale de [Goojet]({{<relref "/expériences/#goojet" >}}) dont il est l'un des cofondateurs. J'ai travaillé pendant 10 années avec Marc au sein de cette société et de la suivante ([scoop.it]({{<relref "/expériences/#scoopit" >}})).


> J'ai eu la chance de travailler avec Jean-Baptiste qui m'avait été recommandé pour bâtir l'équipe de la société Goojet dont je suis un des cofondateurs. Jean-Baptiste a travaillé avec moi pendant 10 ans au sein de cette société et de la suivante, Scoop.it, dont il a toujours été un élément clef. Ses forces principales : compétence technique, éthique du plus haut niveau, créativité, initiative. Jean-Baptiste est aussi une personne engagée qui cherche et donne du sens à son travail : quand il travaille pour une cause en laquelle il croit, il donne 200% de son talent. Je retravaillerais avec Jean-Baptiste à la première occasion, sans hésitation.


### Laurent Gence
[{{% fontawesome linkedin %}}](https://www.linkedin.com/in/lgence/)
**Tech Lead Frontend** @ *[Linkfluence](https://www.linkfluence.com/)*

La rencontre avec Laurent est celle d'un rachat : [Linkfluence]({{<relref "/expériences/#linkfluence">}}) rachetant [scoop.it]({{<relref "/expériences/#scoopit" >}}) ([Linkfluence lève 18 millions d’euros et s’offre Scoop.it](https://www.frenchweb.fr/linkfluence-leve-18-millions-deuros-et-soffre-scoop-it/337061)). J'intègre donc son équipe Parisienne tout en restant à Toulouse. 
> Il est rare de rencontrer une personne comme Jean-Baptiste qui conjugue autant autonomie et expertise technique. J'ai eu la chance de travailler avec lui au sein de Linkfluence sur la création d'une application web complexe. J’ai été particulièrement impressionné par la capacité de Jean-Baptiste à porter ce projet à distance et à être moteur sur les différents sujets : architecture, design, gestion de projet et mentoring d'équipe. Jean-Baptiste sera un vrai atout pour ceux qui cherchent une personne pouvant gérer un projet web de façon autonome et avec une exigence de qualité.
