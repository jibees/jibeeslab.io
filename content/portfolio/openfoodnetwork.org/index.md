---
title: Core developer pour OpenFoodNetwork
description: Développement de fonctionnalités et de correctifs pour le logiciel libre Open Food Network.
date: 2022-11-01
jobDate: 2020-2022
work: [RoR, AngularJS, Github, Docker, Ansible]
volunteers: false
thumbnail: openfoodnetwork.org/ofn.png
projectUrl: https://github.com/openfoodfoundation
---

# Open Food Network

**Participation à la conception et au développement de la plateforme de vente en ligne pour les circuits courts alimentaires.**

Projet libre, Open Food Network est une plateforme de vente en ligne pour les circuits courts alimentaires. Elle permet aux producteurs de vendre directement à leurs clients, et aux clients de trouver les producteurs locaux qui leur conviennent.

_Environnement logiciel libre_ : travail en équipe, code ouvert et libre, tests automatisés, intégration continue, review de code (des membres de l'équipe mais également de contributeurs/contributrices), souci du détail dans la création de commits/pull requests.

_Environnement technique_ : Ruby on Rails, AngularJS, PostgreSQL, Docker, Ansible, Github CI/Actions.

_Environnement gestion de projet_ : Github, Slack, travail en anglais à distance avec des équipes internationales, réunions hebdomadaires en anglais.

En savoir plus:

- Lien vers le projet OpenFoodNetwork sur [Github](https://github.com/openfoodfoundation)
- Liste des pull requests mergées sur [Github](https://github.com/openfoodfoundation/openfoodnetwork/issues?q=author%3Ajibees+is%3Aclosed)
