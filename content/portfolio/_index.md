---
title: Portfolio
description: "Mes réalisations"
---

Je présente ici les travaux que j'ai effectués dans le cadre de ma reconversion professionnelle uniquement ([voir mon expérience professionnelle]({{<relref "/expériences/" >}})).

Ces réalisations sont assez variées (pédagogie, développement web, communication, design, ...) mais toujours pour une mission à laquelle je crois.

Aidez-moi à construire cette partie, je suis [disponible](mailto:jb.bellet@gmail.com).
