---
title: Développeur web pour CoopCircuits.fr
description: Développement d'outils spécifiques & libres pour les circuits courts alimentaires
date: 2022-11-02
jobDate: 2020-2022
work: [React.JS, next.js, Docker, Gitlab CI/CD]
volunteers: false
thumbnail: coopcircuits.fr/coopcircuits.png
projectUrl: https://coopcircuits.fr/
---

# CoopCircuits

**Participation à la conception et au développement d'outils libres pour les circuits courts alimentaires.**

CoopCircuits est une Société Coopérative d'Intéret Collectif qui accompagne les circuits courts alimentaires dans leur développement et en particulier leur développement numérique. Elle concoit et mets à disposition des outils libres pour les organisateurs et organisatrices de circuits courts alimentaires.

_Environnement logiciel libre_ : travail en équipe, code ouvert et libre, tests automatisés, intégration continue, souci du détail dans la création de commits/pull requests.

_Environnement technique_ : React.JS, next.js, Gitlab CI/CD, Docker

_Environnement gestion de projet_ : Gitlab, Slack

En savoir plus:

- Lien vers les projets CoopCircuits sur [Gitlab](https://gitlab.com/coopcircuits)
