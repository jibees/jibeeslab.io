---
title: Formulaire porteurs de projets agricoles
description: Création d'un formulaire et de sa base de données synchronisée pour les porteurs de projets agricoles
date: 2020-05-25
jobDate: 2020
volunteers: true
work: [typeform, airtable, zapier]
thumbnail: le100esinge/le100esinge.jpg
projectUrl: https://www.facebook.com/le100esinge/posts/1141759926202872
testimonial:
  name: Amandine Largeaud
  role: Co-fondatrice et co-gérante chez Le Labo du 100e Singe, accélérateur de nouveau(x) monde(s)
  image: le100esinge/amandine-largeaud.jpg
  text: La collaboration avec Jean-Baptiste nous a permis de mettre en place une base de données essentielle pour l'impulsion de nos activités de transition dans le domaine agricole. Techniquement très pointu, d'une grande réactivité, avec une aisance dans la collaboration à distance, une adaptation à notre contexte et une compréhension des enjeux que nous adressons, ce travail ensemble a été en tout point une réussite.
---


Création d'un formulaire et de sa base de données synchronisée pour les porteurs de projets agricoles en péri-urbain.

 - Travail en amont autour des questions de RGPD et d'éthique
 - Conception du formulaire en ligne pour les porteurs de projet agricole
 - Design de la base de données et de sa synchronisation avec l'outil de formulaire en ligne
 - Travail pédagogique pour expliquer le fonctionnement et l'utilisation de la solution auprès des commanditaires