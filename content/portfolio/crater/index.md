---
title: Contributeur irrégulier pour CRATer
description: CRATer, un outil de diagnostic au service de la transition agro-alimentaire des territoires
date: 2023-01-01
jobDate: depuis 2020
volunteers: true
work: [gitlab, react, typescript, webcomponents, cypress, lit]
thumbnail: crater/crater.png
projectUrl: https://framagit.org/lga/crater-ui
---

Outil de diagnostic au service de la transition agro-alimentaire des territoires

Mes contributions: 
 - https://framagit.org/lga/crater-ui/-/merge_requests/557
 - https://framagit.org/lga/crater-ui/-/merge_requests/49


