---
title: Création d'un site web
description: Création du site internet de campagne pour les élections municipales avec hugo.
date: 2020-05-17
jobDate: 2019-2020
work: [design, creation web]
volunteers: true
thumbnail: castanetencommun.org/castanetencommun.jpg
projectUrl: https://www.castanetencommun.org
testimonial:
  name: Lucia Vidal-Checa
  role: Co-directrice de campagne
  image: castanetencommun.org/lucia-vidal-checa.jpg
  text: En tant que co-directrice de campagne, j'ai eu l'occasion de travailler conjointement avec Jean-Baptiste sur le développement et l'animation de notre site internet, outil indispensable de la communication vers les citoyens de notre commune. J'ai pu apprécier sa réactivité, son engagement et sa capacité à prendre des initiatives, tout en analysant et anticipant les demandes que nous avions. Jean-Baptiste a été une personne essentielle dans le processus de développement de notre collectif, son soutien a été précieux notamment autour des questions de gouvernance interne et d'intelligence collective. De fait, je pense que les qualités personnelles et professionnelles de Jean-Baptiste peuvent être, sans aucun doute, de véritables atouts pour votre structure.
---


Création du site internet du collectif Castanet en Commun en vue des élections municipales de 2020.

 - Définition des besoins.
 - Implémentation avec le CMS https://gohugo.io/ et déploiement continue sur https://gitlab.com/ Pages.