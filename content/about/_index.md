---
title: A propos
description:
images: ["/images/opengraph.jpg"]
---

Vous trouverez ici des précisions sur mon CV concernant les critères importants que je souhaite mettre en valeur. Ces critères ne sont pas à considérer comme un caprice personnel, mais bien comme une urgence répondant à la situation critique et systémique à laquelle nous faisons face.

## Les transitions

Après avoir été formé aux enjeux globaux au sein du [Mastère Spécialisé Eco-ingénierie](<{{< relref "/formations#inp-toulouse" >}}>) de l'INP Toulouse, je désire orienter mon projet professionnel vers les transitions :

- **agriculture/alimentation** : opérer les changements sur le monde agricole ; santé des sols, des plantes, des animaux ; rapprocher les consommateurs et producteurs
- **climat & énergie** : transformer la société et sa dépendance énergétique face à la double contrainte énergie-climat
- **éducation** : l'éducation pour tou.te.s ; faire comprendre les enjeux ; questionner la technique et son rôle sociétal
- **biodiversité** : face à la sixième extinction de masse, prendre soin des non-humains
- **santé & social** : prendre soin de tou.te.s, dans la diversité
- **démocratique** : redonner de la place et le pouvoir de décision aux citoyens : éducation populaire, formation, vie politique, ...

Vu l'urgence à laquelle nous sommes confrontés, il faut être précis et viser les domaines à impact fort.

## De l'importance de la structure

J'accorde une importance particulière aux types de structure pour lesquels je travaille : en priorité les associations et les coopératives. En faisant le choix de confier le pouvoir de décisions aux salariés, ces coopératives permettent l'émancipation des travailleurs. De même, la répartition de bénéfices est encadrée et ne vise pas l'accumulation par les actionnaires. Pouvoir démocratique aux salariés, autonomie et indépendance, ... sont autant d'atouts pour réaliser la bifurcation écologique urgente et nécessaire.

## Le choix du télétravail

Faire le choix du télétravail, c'est ne plus choisir son lieu de vie en fonction de son lieu de travail. Faire le choix du télétravail c'est donc entrer en rupture face à la métropolisation du monde et inventer des nouvelles formes d'habitat, en lien avec la diversité des territoires.

J'ai plusieurs fois fait l'expérience du télétravail (à [Linkfluence]({{<relref "/expériences/#linkfluence">}}), mais aussi pour le [100eSinge]({{< relref "/portfolio/le100esinge/index.md" >}})) : les prérequis du télétravail (communication fluide, nécessité de clarifier, ...) permettent d'avancer dans un cadre sécurisé et rassurant pour tout.e.s (télétravail ou présentiel).

## Réduire son temps de travail

Réduire son temps de travail, c'est augmenter son temps de vie pour réaliser des activités non rémunérées : associatif, vie de la cité, militantisme, soin des autres et de soi-même. Réduire son temps de travail, c'est laisser la possibilité à ceux qui n'ont pas de travail d'en avoir un (et en particulier ceux qui ont eu moins de _chance_ que moi : personne racisée, milieu social, ...). Enfin, réduire son temps de travail, c'est refuser l'accélération du monde.
