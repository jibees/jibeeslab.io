---
title: Mes passions
description: 
images: ["/images/opengraph.jpg"]
---



### La vie de la cité
{{< figure src="/images/castanet-en-commun.jpg" caption="Interview de Xavier Normand, tête de liste du collectif Castanet en Commun" >}}

La vie de la cité, ce qui concerne le citoyen, à l'échelle locale est un levier formidable pour effectuer, impulser un changement nécessaire et urgent. C'est donc à l'occasion des élections municipales 2020 que j'ai pu participer activement à la création et au développement d'un collectif citoyen soutenu par l'ensemble des parties politiques *de gauche*.

Avec des pratiques démocratiques nouvelles (élection sans candidat, jugement majoritaire, réunion avec les principes de l'intelligence collective, ...), ce collectif a su faire émerger un projet cohérent et solide ainsi qu'une équipe extrêmement soudée et unie dans un seul objectif : gagner la bataille des idées autour des 3 valeurs que sont **l'écologie, la démocratique permanente et les solidarités**.

### La montagne
{{< figure src="/images/montagne.jpg" caption="Coucher de soleil dans les Pyrénées" >}}

La montagne est un formidable lieu de contemplation et de construction de nouveaux imaginaires. Dans un rapport d'humilité permanente face au lieu, sans cesse ramené à notre propre précarité, le cheminement est propice à la réflexion, aux discussions croisées, aux regards réflexifs individuels et collectifs. C'est également un lieu d'une grand beauté qui autorise à capturer l'éphémère d'un coucher de soleil au sommet.


### Le sport
{{< figure src="/images/sport.jpg" caption="Raid INSA-INP, Luchon Aneto Trail, Trail du Mourtis" >}}


Le sport est une allégorie à la fois du dépassement de soi, mais aussi de l'entraide. Partager un moment, se nourrir de l'effort de l'autre, s'encourager, profiter, sont des moments qui restent en mémoire pour longtemps.


