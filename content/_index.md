---
title: Accueil
homePageTitle: Développeur informatique pour les transitions
description:
images: ["/images/opengraph.jpg"]
---

Bonjour,

Je m'appelle Jean-Baptiste Bellet, j'ai 37 ans et je suis à la recherche d'un poste en développement (web, informatique, ...) _pour_ les transitions. Après 13 années d'expérience dans le développement web pour les startups, j'ai décidé de mettre mes compétences au service des transitions.

- Je suis actuellement disponible pour **tout type de contrat** (CDI, CDD, freelance).
- Je recherche à apporter ma contribution dans les domaines des **transitions** : agriculture/alimentation, climat & énergie, éducation, biodiversité, santé & social ...
- Je porte une attention particulière à la structure, en privilégiant les associations et **coopératives**.
- Je suis disponible sur la région toulousaine (accessible à vélo depuis Castanet) et en **télétravail**.
- Je suis ouvert au **temps partiel**.

[En savoir plus]({{<relref "/about/" >}})

## Compétences

Treize années de développement web m'ont forgé une connaissance approfondie sur les technologies web (<small>`java, sql, spring, ...`</small>), et en particulier le `frontend` avec une expertise `typescript/react/redux`. Concernant ces 13 années d'[expériences](<{{< relref "/expériences" >}}>), je vous invite à aller voir les [recommandations](<{{< relref "/recommandations" >}}>) de mes managers

Après m'être formé au sein du [Mastère Spécialisé Eco-ingénierie](<{{< relref "/formations#inp-toulouse" >}}>), j'ai pu élargir mon spectre de connaissances en y ajoutant :

- de la **modélisation** : SIG, simulation des systèmes dynamiques, modélisation des systèmes socio-écologiques, ... ;
- des **méthodes et outils de conception et d'évaluation** : bilan carbone et bilan des émissions de GES réglementaire, analyse de cycle de vie, écotoxicologie, ... ;
- enfin, la **gouvernance et l'économie de la soutenabilité** : organisation et coopération, innovation et soutenabilité, intelligence collective, ...

**Je serai ravi de pouvoir mettre à profit à la fois mon expertise mais aussi ma formation récente afin de résoudre des problèmes systémiques en interdisciplinarité et en coopération pour les transitions.**

N'hésitez pas à me [contacter](mailto:jb.bellet@gmail.com).
